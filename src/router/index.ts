import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'


const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'sprint',
    component: () => import('../views/SprintSetup.vue')
  },
  {
    path: '/invoiceSetup',
    name: 'invoiceSetup',
    component: () => import('../views/InvoiceInformation.vue')
  },
  {
    path: '/finalInvoice',
    name: 'finalInvoice',
    component: () => import('../views/FinalInvoice.vue')
  },
  {
    path: '/emailMessage',
    name: 'emailMessage',
    component: () => import('../views/EmailMessage.vue')
  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
