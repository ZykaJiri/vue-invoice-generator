export const sprintBonusResolver = (completionPercentage: number) => {
    if (completionPercentage < 60) {
        return 0.9;
    }

    if (completionPercentage >= 75) {
        return 1.1;
    }

    if (completionPercentage >= 90) {
        return 1.15;
    }

    if (completionPercentage >= 110) {
        return 1.2;
    }
    return 1;
}