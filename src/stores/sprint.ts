import {defineStore} from 'pinia';

const baseSprint: Sprint = {
        sprintNumber: null,
        hoursWorked: null,
        sprintPercentage: null,
        vacationHours: null,
    }

export interface Sprint {
    sprintNumber: number|null;
    hoursWorked: number|null;
    sprintPercentage: number|null;
    vacationHours: number|null;
}

export const useSprintStore = defineStore({
    id: 'sprint',
    state: () => ({
        sprintData: [{...baseSprint}] as Sprint[],
    }),
    actions: {
        addSprint() {
            this.sprintData.push({...baseSprint});
        },
        removeSprint(index: number) {
            this.sprintData.splice(index, 1);
        }
    },
    getters: {
        getSprintData: (state: any) => state.sprintData,
    }
})