import {defineStore} from 'pinia';
import {useSprintStore} from './sprint';

const sprintStore = useSprintStore();

function sprintPercentageResolver(sprintPercentage: number) {
    if (sprintPercentage < 60) {
        return 0.9;
    }

    if (sprintPercentage >= 75 && sprintPercentage < 90) {
        return 1.1;
    }

    if (sprintPercentage > 90 && sprintPercentage < 110) {
        return 1.15;
    }

    if (sprintPercentage >= 110) {
        return 1.2;
    }
    return 1;
}

const useInvoiceInfoStore = defineStore({
    id: 'InvoiceInfo',
    state: () => ({
        serviceProviderCompanyInfo: {
            address: '',
            companyCity: '',
            companyStreet: '',
            postalCode: '',
            name: '',
            ico: '',
        },
        serviceReceiverCompanyInfo: {
            address: '',
            companyCity: '',
            companyStreet: '',
            postalCode: '',
            name: '',
            ico: '',
        },
        monthlyIncome: 0,
        workDays: 0,
        providerPhone: '',
        bankAccount: '',
        selectedMonth: 6,
        selectedYear: 2022
    }),
    getters: {
        getFinalPrice: (state: any) => {
            const thisMonthHourlyRate = state.monthlyIncome / (state.workDays * 8);
            let finalPrice = 0;
            sprintStore.sprintData.forEach((sprint: any) => {
                const sprintBonus = sprintPercentageResolver(sprint.sprintPercentage);
                finalPrice += sprint.hoursWorked * thisMonthHourlyRate * sprintBonus;
                finalPrice += sprint.vacationHours * thisMonthHourlyRate;
            });
            return Math.floor(finalPrice);
        },
        getMonthHourlyRate: (state: any) => {
            return Math.floor(state.monthlyIncome / (state.workDays * 8) * 100) / 100;
        }
    },
    persist: true
})

export {useInvoiceInfoStore, sprintPercentageResolver};